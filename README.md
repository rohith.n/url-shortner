# Url Shortener

A Java micro service created with the objective of generating short URLs for a given URL.

Documentation Link: [Url-Shortener](https://docs.google.com/document/d/10fgiBdj5DFR1oZe6ORIqsu3bbwvH2zbRyrBuz1TYuk4/edit?usp=sharing)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

1. Java 11
2. IntelliJ IDEA / Eclipse or any editor capable of running Java
3. Install Maven
4. Add Lombok Plugin
5. Install Postgres (for running a pure development environment)

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Postgres](https://www.postgresql.org/) - Persistent Storage