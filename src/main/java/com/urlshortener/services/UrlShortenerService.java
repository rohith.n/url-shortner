package com.urlshortener.services;

import com.urlshortener.models.db.Url;

public interface UrlShortenerService {

    String shortenUrl(String baseUrl, String urlToShorten);

    Url getUrlFromId(String uniqueId);
}
