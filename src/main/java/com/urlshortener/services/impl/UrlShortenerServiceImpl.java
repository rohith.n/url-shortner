package com.urlshortener.services.impl;

import com.urlshortener.exceptions.NotFoundException;
import com.urlshortener.exceptions.ValidationException;
import com.urlshortener.models.db.Url;
import com.urlshortener.repositories.UrlRepository;
import com.urlshortener.services.UrlShortenerService;
import com.urlshortener.utils.IdConverterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UrlShortenerServiceImpl implements UrlShortenerService {

    @Autowired
    public IdConverterUtils idConverterUtils;

    @Autowired
    public UrlRepository urlRepository;

    @Override
    public String shortenUrl(String localUrl, String urlToShorten) {
        validateUniquenessofUrl(urlToShorten);
        Url urlEntity = new Url(urlToShorten);
        urlRepository.save(urlEntity);
        String uniqueUrl = idConverterUtils.createShortUrl(urlEntity.getId());
        String baseString = getBaseUrlFromRequestUrl(localUrl);
        String shortenedUrl = baseString + uniqueUrl;
        urlEntity.setShortUrl(shortenedUrl);
        urlRepository.save(urlEntity);
        return shortenedUrl;
    }

    private String getBaseUrlFromRequestUrl(String localURL) {
        String[] addressComponents = localURL.split("/");
        // remove the endpoint
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < addressComponents.length - 1; ++i) {
            if(i == 1) {
                sb.append("//");
            }
            sb.append(addressComponents[i]);
        }
        sb.append('/');
        return sb.toString();
    }

    @Override
    public Url getUrlFromId(String shortenedUrl) {
        Long id = idConverterUtils.getIdFromShortUrl(shortenedUrl);
        Url urlEntity = urlRepository.findById(id).orElse(null);
        if(null == urlEntity) {
            throw new NotFoundException("no equivalent Url found for this short url");
        }
        return urlEntity;
    }

    private void validateUniquenessofUrl(String urlToShorten) {
        // Best Alternative would be to make use of the In-memory cache like Redis to validate
        boolean urlExists = urlRepository.existsUrlByOriginalUrl(urlToShorten);
        if(urlExists) {
            throw new ValidationException("given url already exists in the database");
        }
    }
}
