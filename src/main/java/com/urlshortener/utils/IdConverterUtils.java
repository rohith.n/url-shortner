package com.urlshortener.utils;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Component
public class IdConverterUtils {

    private HashMap<Character, Integer> charToIndexMap;
    private List<Character> indexToCharMap;

    @PostConstruct
    public void initialize() {

        //initialize CharToIndexMap
        charToIndexMap = new HashMap<>();
        for (int i = 0; i < 26; ++i) {
            char c = 'a';
            c += i;
            charToIndexMap.put(c, i);
        }
        for (int i = 26; i < 52; ++i) {
            char c = 'A';
            c += (i-26);
            charToIndexMap.put(c, i);
        }
        for (int i = 52; i < 62; ++i) {
            char c = '0';
            c += (i - 52);
            charToIndexMap.put(c, i);
        }

        //Initialize indexToCharList
        indexToCharMap = new LinkedList<>();
        for (int i = 0; i < 26; ++i) {
            char c = 'a';
            c += i;
            indexToCharMap.add(c);
        }
        for (int i = 26; i < 52; ++i) {
            char c = 'A';
            c += (i-26);
            indexToCharMap.add(c);
        }
        for (int i = 52; i < 62; ++i) {
            char c = '0';
            c += (i - 52);
            indexToCharMap.add(c);
        }
    }

    private List<Integer> convertFromBase10To62(Long id) {
        LinkedList<Integer> digits = new LinkedList<>();
        while(id > 0) {
            int remainder = (int)(id % 62);
            digits.addFirst(remainder);
            id = id/62;
        }
        return digits;
    }

    // id = 2 (base10) -> c (base62) <-> 2 (EquivalentIndex)
    // i = 0, n = 0 -> 2 * 62^0

    // id = 124 (base10) -> ca (base62) <-> 20 (EquivalentIndex)
    // i = 0, n = 1 -> 2 * 62^1
    // i = 1, n = 0 -> 0 * 62^0

    // id = 125 (base10) -> cb (base62) <-> 21 (EquivalentIndex)
    // i = 0, n = 1 -> 2 * 62^1
    // i = 1, n = 0 -> 1 * 62^0

    // id = 7688 (base10) -> caa (base62) <-> 200 (EquivalentIndex)
    // i = 0, n = 2 -> 2 * 62^2
    // i = 1, n = 1 -> 0 * 62^1
    // i = 2, n = 0 -> 0 * 62^0

    private Long convertFromBase62To10(List<Character> ids) {
        long id = 0L;
        for (int i = 0, n = ids.size() - 1; i < ids.size(); ++i, --n) {
            int base10 = charToIndexMap.get(ids.get(i));
            id += (base10 * Math.pow(62.0, n));
        }
        return id;
    }

    public String createShortUrl(Long id) {
        List<Integer> base62Digits = convertFromBase10To62(id);
        StringBuilder shortUrlBuilder = new StringBuilder();
        for(int digit : base62Digits) {
            shortUrlBuilder.append(indexToCharMap.get(digit));
        }
        return shortUrlBuilder.toString();
    }

    public Long getIdFromShortUrl(String shortUrl) {
        List<Character> base62IDs = new ArrayList<>();
        for (int i = 0; i < shortUrl.length(); ++i) {
            base62IDs.add(shortUrl.charAt(i));
        }
        return convertFromBase62To10(base62IDs);
    }
}
