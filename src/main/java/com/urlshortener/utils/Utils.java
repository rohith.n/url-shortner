package com.urlshortener.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static Date getCurrentDate() {
        return formatDate(new Date(), "dd-MM-yyyy HH:mm:ss");
    }

    public static Date formatDate(Date date, String format) {
        try {
            date = convertStringToDate(getFormattedDate(date, format), format);
        } catch (ParseException e) {

        }
        return date;
    }

    public static Date convertStringToDate(String date, String format) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        return dateFormat.parse(date);
    }

    public static String getFormattedDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        return dateFormat.format(date);
    }
}
