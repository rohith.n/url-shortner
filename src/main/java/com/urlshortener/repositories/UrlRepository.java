package com.urlshortener.repositories;

import com.urlshortener.models.db.Url;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UrlRepository extends JpaRepository<Url, Long> {

    boolean existsUrlByOriginalUrl(String originalUrl);
}
