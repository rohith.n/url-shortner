package com.urlshortener.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import java.io.Serializable;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> implements Serializable {

    private boolean success;

    private String message;

    private T data;

    public Response(String message) {
        // All error responses
        this(false, message);
    }

    public Response(boolean success, T data, String message) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public Response(T data, String message) {
        // All success responses
        this.success = true;
        this.data = data;
        this.message = message;
    }

    public Response(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}
