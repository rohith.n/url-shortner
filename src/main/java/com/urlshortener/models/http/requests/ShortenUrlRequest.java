package com.urlshortener.models.http.requests;

import com.urlshortener.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.net.URL;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class ShortenUrlRequest {

    private String url;

    public static void validateUrl(String url) {
        try {
            URL obj = new URL(url);
            obj.toURI();
        } catch (Exception e) {
            throw new ValidationException("Invalid url provided");
        }
    }
}
