package com.urlshortener.models.http.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
public class ShortenUrlResponse implements Serializable {

    private String url;

    private String shortenUrl;
}
