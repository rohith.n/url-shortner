package com.urlshortener.models.db;

import com.urlshortener.utils.Utils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Url")
@NoArgsConstructor
@Getter
public class Url {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String originalUrl;

    @Setter
    private String shortUrl;

    private Date createdAt;

    public Url(String originalUrl) {
        this.originalUrl = originalUrl;
        this.createdAt = Utils.getCurrentDate();
    }
}
