package com.urlshortener.exceptions;

import com.urlshortener.models.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class);

    private ResponseEntity<Response> handleException(String message, HttpStatus httpStatus) {
        Response errorResponse = new Response(message);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Response> handleNotFoundExceptions(NotFoundException ex, WebRequest request) {
        return handleException(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<Response> handleValidationExceptions(ValidationException ex, WebRequest request) {
        return handleException(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Response> handleAllExceptions(Exception ex) {
        LOGGER.error("[handleAllExceptions] exception occurred for request body " + ex);
        return handleException("Internal Server Error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
