package com.urlshortener.exceptions;

public class ValidationException extends RuntimeException {
    public ValidationException(String ex) { super(ex);}
}
