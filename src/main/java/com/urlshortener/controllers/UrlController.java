package com.urlshortener.controllers;

import com.urlshortener.models.Response;
import com.urlshortener.models.db.Url;
import com.urlshortener.models.http.requests.ShortenUrlRequest;
import com.urlshortener.models.http.response.ShortenUrlResponse;
import com.urlshortener.services.UrlShortenerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;


@RestController
public class UrlController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UrlController.class);

    @Autowired
    private UrlShortenerService urlShortenerService;

    @RequestMapping(value = "/shorten", method = RequestMethod.POST)
    public ResponseEntity<Response<ShortenUrlResponse>> shortenUrl(@RequestBody ShortenUrlRequest shortenUrlRequest, HttpServletRequest request) {
        LOGGER.info("[shortenUrl] request received: " + shortenUrlRequest);
        String urlToShorten = shortenUrlRequest.getUrl();
        ShortenUrlRequest.validateUrl(urlToShorten);
        String baseUrl = request.getRequestURL().toString();
        String shortenedUrl = urlShortenerService.shortenUrl(baseUrl, urlToShorten);
        ShortenUrlResponse response = new ShortenUrlResponse(urlToShorten, shortenedUrl);
        LOGGER.info("[shortenUrl] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response, "shortened url created successfully"), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public RedirectView redirectUrl(@PathVariable String id) {
        LOGGER.info("[redirectUrl] request received: " + id);
        Url url = urlShortenerService.getUrlFromId(id);
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(url.getOriginalUrl());
        return redirectView;
    }

    @RequestMapping(value = "/v1/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<ShortenUrlResponse>> getOriginalUrl(@PathVariable String id) {
        LOGGER.info("[getOriginalUrl] request received: " + id);
        Url url = urlShortenerService.getUrlFromId(id);
        ShortenUrlResponse response = new ShortenUrlResponse(url.getOriginalUrl(), url.getShortUrl());
        return new ResponseEntity<>(new Response<>(response, "shortened url obtained successfully"), HttpStatus.OK);
    }
}
